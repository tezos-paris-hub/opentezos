---
id: consensus-mechanism
title: Consensus Mechanism
authors: Mathias Hiron and Lucas Felli
---

In the previous modules, we introduced an architecture based on a peer-to-peer network that can be leveraged to store data and handle a cryptocurrency, with all the benefits of decentralization. We presented how we can use a sequence of transactions between accounts, grouped into a sequence of blocks.

One key aspect we still need is some way for this network to agree on which transactions should be in the next block, and in which order.

The mechanism used to reach such an agreement is called the **consensus mechanism**.

### Centralized production of blocks

As the goal of grouping transactions in blocks is to avoid exchanging messages for every transaction, proposing a next block is not something that can reasonably be done collectively.

One of the nodes has to take this responsibility, select a set of transactions among the ones it has received since the last block, define an order for these transactions, and group them in a block. This block can then be propagated through the p2p network, and the process of having the community agree that this should be the next block, can then take place.

On Tezos, we call the entity that creates a block a baker. On other blockchains, it can be called a miner, or validator.

As one entity, a baker, gets to create block, this implies some centralization, with the issues that come with it. In particular, the baker may:

- censor some transactions
- give priority to other transactions
- entirely fail to do its job and not create any block

For the first two points, it may be for a number of reasons, such as favoring entities it likes, or simply censor or give priority to transactions depending on whether it benefits from it.

To avoid the issues that come with this centralization and the power a baker has, the solution is to distribute this power and responsibility over time: **for each new block, we select a new baker**.

### The difficulty of selecting a baker

For each block, a baker has to be somehow selected, so that the responsibility of creating the next block is not concentrated on one entity, or on a small number of entities.

Think about what algorithm you would you use to select a baker for each block, so that this power/responsibility is spread fairly among the community, thus ensuring decentralization?

A natural answer would be to randomly select among all the different available bakers, and spread the responsibility evenly. Unfortunately, this doesn't solve the problem. Can you find out why?

The issue is that as a blockchain is public and permissionless, we need anyone to be able to become a baker, and participate in ensuring the security of the blockchain. But if anyone can be a baker, this also means anyone can be multiple bakers. If there are N bakers, then one entity registers as N new bakers, then if we select the next baker randomly among available bakers, this single entity would have one of its bakers selected for half of the blocks on average, and gain too much power.

An attack where some entity manages to accumulate a significant share of power is what we call a **Sybil Attack**. Finding a way to prevent Sybil attacks is at the heart of the problem of blockchains.

Making sure every baker is a unique entity, and that several such entities are not in practice controlled by the same entity, can't be done without a centralized authority in charge of investigating and controlling who can be a baker. This means we can't use a solution that depends on finding out who is behind each baker.

The solution needs to be based on something that entities can't freely create an infinite amount of some kind of resource.

### Proof of Stake

One common resource an entity can't create an infinite amount of is money.

On a blockchain, we can conveniently use the native cryptocurrency, as a way to represent this limited resource. It can be converted from and into other currencies through exchanges.

This gives us way to select the baker for the next block: instead of selecting them randomly and evenly among all registered bakers, we can select them randomly, but in proportion to the amount of native cryptocurrency they possess.

As owning a high proportion of the available tez would be prohibitively expensive, it is unlikely that a single entity would take too much control this way.

Approaches based on how much cryptocurrency an entity has, as a way to distribute the responsibility of creating the next block is called *Proof of Stake*.

This is the approach used by most modern blockchains.

Tezos was one of the first blockchains to use Proof of Stake as the way to assign block creation. Since then, many blockchains adopted this approach.

<!-- TODO: add link to a more detailed page -->

### Proof of Work

Another approach that was selected initially by some blockchains, including the first blockchain, Bitcoin, is to use computing power as the limited resource used to select who gets to create the next block.

The idea is that to have a chance at being the one to produce the next block, you have to perform a large amount of computation. The more computation you can perform in a given amount of time, the more likely you are to produce the next block.

For similar reasons to money, having a high proportion of the computing power dedicated to a blockchain can be very hard and costly to achieve, so this can be effective as a way to distribute the ability to create blocks among many entities.

An example of computation that is being used by some blockchains for PoW, consists in, given some data such as the content of the block being produced, to find a value for some extra piece of data to be included, such that the hash of the resulting data has a certain property. For example, the goal can be to obtain a hash that has its last N bits set to 0. As there is no way to control the output of a cryptographic hash function, other than trying different inputs and computing the output for each of them, the value of N directly impacts the average number of hashes that need to be computed, in order to get the required property.

Proof-of-Work has a very significant drawback: to prove that an entity has significant computing power, this computing power has to be spent, and it is typically spent in a way that has no other benefits. This causes waste of huge amounts of computing power and electricity, with corresponding effects on the environment. A [study](https://www.jbs.cam.ac.uk/2022/a-deep-dive-into-bitcoins-environmental-impact/) from the University of Cambridge estimated that in 2022, the largest PoW cryptocurrency, produced about 0.3% of global annual greenhouse gas emissions.

This is the main reason why modern blockchains tend to abandon PoW in favor of PoS as a way to distribute the block creation power.

Note that some very minor of PoW can be used in PoS blockchains, as a way to prevent DOS attacks.

### Incentives

As being a baker (or miner) means taking the time to set up and administer one or more servers, and in the case of PoW blockchains, spending significant amount of electricity, there needs to be some incentive to encourage as many people as possible to take (and share) this responsibility.

Being a baker for a given block can give some amount of power. However, as everything is done to limit this amount of power, and taking advantage of this power is not easy, some other, more direct incentive is needed.

A system of rewards is therefore put in place: whoever creates the next block will be rewarded in the native cryptocurrency of the blockchain.

This reward can be composed of:

- some amount of newly minted cryptocurrency
- fees paid by the emitter of transactions

<!-- TODO: add more details? -->

### Slashing

As being in charge of creating the next block is a responsibility, bad behavior has to be deterred:

- producing multiple blocks in a single round
- attesting or pre-attesting multiple blocks in a single round

When using Proof of Stake, instead of simply considering how much bakers own, we let them put some amount of cryptocurrency at stake, temporarily locked, so that if they are proven to badly behave, some of these funds will be taken from them as a punishment. This is what we call slashing.

<!-- TODO: improve -->

### Delegation and staking 

<!-- TODO: Move to the Tezos part -->

To increase decentralization, and as not everyone has enough funds that they can lock in their account, or the ability to set up a server and bake, we need a way for more people to participate in securing the blockchain without these risks and responsibilities.

On Tezos, two possibilities are available to fulfill this purpose: anyone who owns some tez can either **delegate** or **stake** their tez to the baker of their choice. In both cases, the tez contribute to increase the chance of the baker to be selected for the next block.

In exchange for the tez delegated or staked to them, bakers in turn, share a portion of their rewards among delegators and stakers. Each baker chooses how much, creating a competitive market between bakers.

Delegated tez are not locked and not at risk of being slashed, while staked tez are locked and may be slashed. The main advantage of staking is that the reward is twice higher than for delegating (ignoring the edge kept by bakers for themselves). Another advantage is that the reward for staking is paid automatically, while for delegation, the payment is done by the baker.

Only a limited portion (90%) of the tez for a given baker can come from delegation or staking.

For more details about delegation and staking, check the [How baking works](/node-baking/baking/baking_explained/) module.